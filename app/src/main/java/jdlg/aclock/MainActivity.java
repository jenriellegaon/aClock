package jdlg.aclock;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    FloatingActionButton addReminder;
    ConstraintLayout consLayout;
    int mHour;
    int mMinute;
    int selHour;
    int selMinite;
    String AM_PM;
    String selTime;
    String time;
    TextInputLayout tilPname;
    TextInputLayout tilMed;
    TextInputEditText etPname;
    TextInputEditText etMed;
    String Pname;
    String Med;
    Dialog remDialog;
    RecyclerView rv;




    /****************************************************************************************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();

        addReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTime();
            }
        });





    }
    /****************************************************************************************************************************/


    /****************************************************************************************************************************/
    //Set the time from the time picker dialog
    public void setTime() {

        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);



        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        selHour = hourOfDay;
                        selMinite = minute;

                        selTime = selHour + ":" + selMinite;

                        SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm");
                        Date date = null;
                        try {
                            date = sdf1.parse(selTime);
                        } catch (ParseException e) {

                            e.printStackTrace();
                        }

                        SimpleDateFormat sdf2 = new SimpleDateFormat("hh:mm aa");

                        time = sdf2.format(date);


                        if(hourOfDay < 12) {
                            AM_PM = "AM";
                        } else {
                            AM_PM = "PM";
                        }

                        setReminder();
                    }

                }, mHour, mMinute, false);
        timePickerDialog.setTitle("Set Alarm Time");
        timePickerDialog.show();
    }
    /****************************************************************************************************************************/


    /****************************************************************************************************************************/
    //Set the reminder
    public void setReminder() {

        remDialog = new Dialog(MainActivity.this);
        remDialog.setCancelable(true);
        remDialog.setTitle("Set Reminder");
        remDialog.setCanceledOnTouchOutside(false);

        remDialog.setContentView(R.layout.dialog_reminder);
        remDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        final WindowManager.LayoutParams[] lp = {remDialog.getWindow().getAttributes()};
        lp[0].dimAmount = 0.7f;
        remDialog.getWindow().setAttributes(lp[0]);

        final TextView tvTime = remDialog.findViewById(R.id.tvTime);
        tvTime.setText(time);

        tilPname = remDialog.findViewById(R.id.tilPname);
        tilMed = remDialog.findViewById(R.id.tilMed);

        etPname = remDialog.findViewById(R.id.etPname);
        etPname.setFilters(new InputFilter[] {
                new InputFilter() {
                    @Override
                    public CharSequence filter(CharSequence cs, int start,
                                               int end, Spanned spanned, int dStart, int dEnd) {
                        // TODO Auto-generated method stub
                        if(cs.equals("")){ // for backspace
                            return cs;
                        }
                        if(cs.toString().matches("[a-zA-Z ]+")){
                            return cs;
                        }
                        return "";
                    }
                }
        });


        etMed = remDialog.findViewById(R.id.etMed);
        etMed.setFilters(new InputFilter[] {
                new InputFilter() {
                    @Override
                    public CharSequence filter(CharSequence cs, int start,
                                               int end, Spanned spanned, int dStart, int dEnd) {
                        // TODO Auto-generated method stub
                        if(cs.equals("")){ // for backspace
                            return cs;
                        }
                        if(cs.toString().matches("[a-zA-Z ]+")){
                            return cs;
                        }
                        return "";
                    }
                }
        });



        final Button btnSet = remDialog.findViewById(R.id.btnSet);
        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            checkFields();

            }
        });


        remDialog.show();
        remDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        lp[0] = remDialog.getWindow().getAttributes();
        lp[0].dimAmount = 0.7f;
        remDialog.getWindow().setAttributes(lp[0]);
    }
    /****************************************************************************************************************************/


    /****************************************************************************************************************************/
    //Check if fields are empty or not
    public void checkFields() {

        Pname = etPname.getText().toString().trim();
        Med = etMed.getText().toString().trim();

        if(!checkPname()) {
            return;
        }

        if(!checkMed()) {
            return;
        }

        tilPname.setErrorEnabled(false);
        tilMed.setErrorEnabled(false);

        remDialog.dismiss();

        Toast.makeText(getApplicationContext() , Pname + System.getProperty("line.separator") + Med + System.getProperty("line.separator") + time , Toast.LENGTH_LONG).show();









    }
    /****************************************************************************************************************************/


    /****************************************************************************************************************************/
    private boolean checkPname() {
        Pname = etPname.getText().toString().trim();
        if (Pname.isEmpty()) {

            tilPname.setErrorEnabled(true);
            etPname.setError(getString(R.string.empty_field_error));
            requestFocus(tilPname);
            return false;
        }


        tilPname.setErrorEnabled(false);
        return true;
    }
    /****************************************************************************************************************************/


    /****************************************************************************************************************************/
    private boolean checkMed() {
        Med = etMed.getText().toString().trim();
        if (Med.isEmpty()) {

            tilMed.setErrorEnabled(true);
            etMed.setError(getString(R.string.empty_field_error));
            requestFocus(tilMed);
            return false;
        }

        tilMed.setErrorEnabled(false);
        return true;
    }
    /****************************************************************************************************************************/


    /*********************************************************************************************************************/
    //Gives focus on the field
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    /*********************************************************************************************************************/







    /****************************************************************************************************************************/
    //Delete Reminder/Alarm
    public void deleteReminder() {

                Snackbar snackbar = Snackbar
                        .make(consLayout, "Deleted", Snackbar.LENGTH_LONG)
                        .setAction("UNDO", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Snackbar snackbar1 = Snackbar.make(consLayout, "Restored", Snackbar.LENGTH_SHORT);
                                snackbar1.show();
                            }
                        });
                snackbar.show();
    }
    /****************************************************************************************************************************/


    /****************************************************************************************************************************/
    public void initViews() {

        addReminder = findViewById(R.id.fabAddReminder);
        consLayout = findViewById(R.id.cLayout);
        rv = findViewById(R.id.rvAlarmList);
        rv.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(layoutManager);


    }
    /****************************************************************************************************************************/


}
